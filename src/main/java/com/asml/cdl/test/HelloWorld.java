package com.asml.cdl.test;

/**
 * Hello World for new CDL Pipeline experiments
 *
 * @author eguta
 */
public class HelloWorld {

    private HelloWorld() {
    }

    public static void main(String[] args) {
        System.out.println("Simple Hello World Application");
    }

}
