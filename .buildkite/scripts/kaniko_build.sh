#!/bin/bash
set -euox pipefail

DOCKER_REPO="${DOCKER_REPO:-docker-local}"
IMAGE_NAME=cdl-hello-world
IMAGE_TAG="${BUILDKITE_TAG:-latest}"
BUILD_CONTEXT="/workspace/$BUILDKITE_AGENT_NAME/$BUILDKITE_ORGANIZATION_SLUG/$BUILDKITE_PIPELINE_SLUG/$BUILDKITE_JOB_ID/retry$BUILDKITE_RETRY_COUNT/repo"

/usr/local/bin/kaniko-executor \
    --skip-tls-verify \
    --skip-tls-verify-pull \
    --destination $REGISTRY/$DOCKER_REPO/$IMAGE_NAME:$IMAGE_TAG \
    --context $BUILD_CONTEXT