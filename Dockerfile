# Build the OpenJDK image from Dockerhub
FROM cdl-hadoop-artifactory.asml.com/docker/openjdk:13-jdk-alpine

WORKDIR /src

# Copy the content of the source directory into the container at /src
RUN buildkite-agent artifact download hello-world*.jar /src --step :maven: :package:

# Execute the Java code
RUN java -jar hello-world*.jar