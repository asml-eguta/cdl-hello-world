#!/bin/bash

printf "Username: "
read user
printf "Password: "
read -s password

token=$(
    curl -u $user:$password -XPOST "https://cdl-hadoop-artifactory.asml.com/artifactory/api/security/token" -d "username=$user" 
    )

printf "\n$token\n"
